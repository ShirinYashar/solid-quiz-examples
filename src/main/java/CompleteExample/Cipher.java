package CompleteExample;

public interface Cipher {
    public abstract String encrypt(String textToEncrypt);
    public abstract String decrypt(String textToDecrypt);
}
